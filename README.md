# Android Hectre Application

There are 2 jobs: 
A)Pruning
  - 2 staff
B)Thinning
  - 3 staff

For each staff you have:
1) Their first name and last name
2) A rate type that you select between Piece rate and Wages. 
3) If wages is selected, you show a label with "job_name will be paid by wages in this timesheet
4) If piece rate is selected, you show a rate field with a button Apply to all that, if pressed, will set the same rate to all the other
staff under the same job.
5) A collection view with some numbers. Those numbers represent the rows in a field. When you
select a row (so one of those numbers), you get a new label + textfield. In the text field you can
see something like 2 / 556. 2 represents the current number of trees you pruned or thinned
(depending on the job) out of the 556 trees available on the row. 
6) Top of each job there is a ADD MAX TREES button. This one set the number oftrees to its maximum for each job for each staff.
7) Sometimes on the row in the collection view you can see a tiny orange dot in the cell. This means that some trees were
already pruned on that row from another job. For example for Barco, the row 4 has an orange
dot and below the text field, there is a label Yi Wan (250). This means that 250 trees out of 556
trees were already pruned through another job on that row. Which means that for the current job
you cannot go above 556 - 250 = 306 trees.
8) Added confirm update to update values for rateType, rows assign or unassign



**Test Case # Test Case Description Test Steps Expected Result Test Data**

1 Assign new row to user

1) Scroll to the second user. e.g.
Henry.
2) Tap the first unactive row
button (square button with
number inside).

1) A new row field (row label +
textfield + completed label) will
appear below the row buttons.
2) The new row field needs to be
ordered.

2 Unasign row

1) Scroll to the second user. e.g.
Henry.
2) Active all the row buttons.
3) Deactive all the row buttons.

1) All the row fields (row label +
textfield + completed label) will
dissapear from below the row
buttons.
2) The space between users must
collapse (no empty space after
remove the row).


3 Set string on textfield

1) Scroll to the first empty row
field.
2) Tap on it.
3) Write a string value. e.g.
"TEST".

1) No string characters will appear
on the textfield. Screen mockup provided

4 set rate for all picking activities

1) Set piece rate for all the
Pruning users.
2) Set rate 345 for the first
pruning user.
3) Tap on APPLY TO ALL button.

1) All the Pruning users must
have 345 / hour.
2) The rate for the Thining users
will not change.

5 Add max trees

1) Assign the same row button
for all the pruning users (e.g. row
4 for Barco and Henry).
2) Tap on ADD MAX TREES
button on Pruning header.

1) The remaining trees needs to
be splited into all the prunings
users.
e.g. For row 4:
missing trees = total trees -
previous completed trees
missing trees = 556 - 250 = 306
trees per user (value on the
textfield) = missing trees / users
with row 4 assigned
trees per user = 306 / 2
2) The value for thining users will
not change. e.g. Row 4 for Darijan
will not change.

6. Add confirm update
1) select wages or price
2) select or unselect rows 
3) click confirm update

