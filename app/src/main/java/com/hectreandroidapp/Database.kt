package com.hectreandroidapp

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteConstraintException
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import java.util.*

/*
 * Created Database for creating table and adding data to it
 */
class Database(context: Context) :
    SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {

    private var mSqLiteDatabase: SQLiteDatabase? = null

    companion object {
        private const val DATABASE_VERSION = 1
        private const val DATABASE_NAME = "JobsDatabse"

        const val TABLE_JOBS = "table_jobs"
        const val ID = "id_"
        const val NAME_FIRST_LAST = "name_first_last"
        const val STAFF_ID = "staff_id"
        const val RATE_TYPE = "rate_type"
        const val PRICE = "price"
        const val ROW_SELECT_COUNT = "row_select_count"
        const val JOB_TYPE = "job_type"

        const val TABLE_ROWS = "table_rows"
        const val ROW_ID = "row_id"
        const val MAX_TRESS = "max_tress"
        const val ALREADY_PRUNED = "already_pruned"
        const val ALREADY_THINNED = "already_thinned"
        const val ASSIGN = "assign"
        const val NAME = "name"

        private var database: Database? = null
        fun getInstance(context: Context): Database {
            if (database != null) {
                return database as Database
            }
            database = Database(context)
            return database as Database
        }
    }

    override fun onCreate(db: SQLiteDatabase?) {
        db!!.execSQL(
            "CREATE TABLE IF NOT EXISTS " + TABLE_JOBS + "("
                    + ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + STAFF_ID + " VARCHAR ,"
                    + NAME_FIRST_LAST + " VARCHAR,"
                    + RATE_TYPE + " VARCHAR,"
                    + PRICE + " VARCHAR,"
                    + ROW_SELECT_COUNT + " VARCHAR,"
                    + JOB_TYPE + " VARCHAR)"
        )
        db.execSQL(
            "CREATE TABLE IF NOT EXISTS " + TABLE_ROWS + "("
                    + STAFF_ID + " VARCHAR ,"
                    + ROW_ID + " VARCHAR,"
                    + MAX_TRESS + " VARCHAR ,"
                    + ALREADY_PRUNED + " VARCHAR,"
                    + ALREADY_THINNED + " VARCHAR,"
                    + ASSIGN + " VARCHAR,"
                    + JOB_TYPE + " VARCHAR,"
                    + NAME + " VARCHAR)"
        )
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {

    }

    /*
    * @addJob()
    * used to insert data of job details
    * */
    @Throws(SQLiteConstraintException::class)
    fun addJob(jobsDTO: JobsDTO) {

        val values = ContentValues()
        values.put(STAFF_ID, jobsDTO.staffID)
        values.put(NAME_FIRST_LAST, jobsDTO.firstLastName)
        values.put(RATE_TYPE, jobsDTO.rateType)
        values.put(PRICE, jobsDTO.price)
        values.put(ROW_SELECT_COUNT, jobsDTO.rows)
        values.put(JOB_TYPE, jobsDTO.jobType)

        open()
        mSqLiteDatabase?.insertOrThrow(TABLE_JOBS, null, values)
        close()
    }

    /*
  * @addRow()
  * used to insert data of row details
  * */
    @Throws(SQLiteConstraintException::class)
    fun addRow(jobsDTO: JobsDTO) {

        val values = ContentValues()
        values.put(STAFF_ID, jobsDTO.staffID)
        values.put(ROW_ID, jobsDTO.rowID)
        values.put(MAX_TRESS, jobsDTO.maxTrees)
        values.put(ALREADY_PRUNED, jobsDTO.alreadyPruned)
        values.put(ALREADY_THINNED, jobsDTO.alreadyThinned)
        values.put(ASSIGN, jobsDTO.assign)
        values.put(JOB_TYPE, jobsDTO.jobType)
        values.put(NAME, jobsDTO.name)
        open()
        mSqLiteDatabase?.insertOrThrow(TABLE_ROWS, null, values)
        close()
    }

    /*
 * @applyToAllStaff()
* used to update price for the staff for a particular job selected
 * */
    @Throws(SQLiteConstraintException::class)
    fun applyToAllStaff(jobsDTO: JobsDTO) {

        val values = ContentValues()
        values.put(PRICE, jobsDTO.price)
        open()
        try {
            mSqLiteDatabase?.update(
                TABLE_JOBS,
                values,
                "$JOB_TYPE =?",
                arrayOf(jobsDTO.jobType + "")
            )
        } catch (e: Exception) {
        } finally {
            close()
        }
    }

    /*
* @addMaxTrees()
* used to update max tress for the rows selected
* */
    @Throws(SQLiteConstraintException::class)
    fun addMaxTrees(jobsDTO: JobsDTO) {

        val values = ContentValues()
        values.put(MAX_TRESS, jobsDTO.maxTrees)
        open()
        try {
            mSqLiteDatabase?.update(
                TABLE_ROWS,
                values,
                "$ROW_ID =? AND $JOB_TYPE =?",
                arrayOf(jobsDTO.rowID, jobsDTO.jobType)
            )
        } catch (e: Exception) {
        } finally {
            close()
        }
    }

    /*
* @updateJob()
* used to update job data
* */
    @Throws(SQLiteConstraintException::class)
    fun updateJob(jobsDTO: JobsDTO) {

        val values = ContentValues()
        values.put(RATE_TYPE, jobsDTO.rateType)
        if (jobsDTO.rateType == "price") {
            values.put(RATE_TYPE, jobsDTO.rateType)
        }
        if (jobsDTO.rateType == "wages") {
            values.put(RATE_TYPE, jobsDTO.rateType)
        }
        open()
        try {
            mSqLiteDatabase?.update(
                TABLE_JOBS,
                values,
                "$STAFF_ID =? AND $JOB_TYPE =?",
                arrayOf(jobsDTO.staffID, jobsDTO.jobType)
            )
        } catch (e: Exception) {
        } finally {
            close()
        }
    }

    /*
* @updateRow()
* used to update row data
* */
    @Throws(SQLiteConstraintException::class)
    fun updateRow(jobsDTO: JobsDTO) {

        val values = ContentValues()
        values.put(ASSIGN, jobsDTO.assign)
        open()
        try {
            mSqLiteDatabase?.update(
                TABLE_ROWS, values, "$STAFF_ID =? AND $JOB_TYPE =? AND $ROW_ID =?",
                arrayOf(jobsDTO.staffID, jobsDTO.jobType, jobsDTO.rowID)
            )
        } catch (e: Exception) {
        } finally {
            close()
        }
    }

    /*
* @getJobList()
* used to fetch job list based on jobType
* */
    @Throws(SQLiteConstraintException::class)
    fun getJobList(jobType: String?): ArrayList<JobsDTO>? {
        val jobDTOArrayList: ArrayList<JobsDTO> = ArrayList<JobsDTO>()
        open()
        var cursor: Cursor? = null
        try {
            cursor = mSqLiteDatabase!!.rawQuery(
                "SELECT * FROM $TABLE_JOBS WHERE $JOB_TYPE =?",
                arrayOf(jobType)
            )
            if (cursor != null) {
                if (cursor.count != 0) {
                    cursor.moveToFirst()
                    do {
                        val jobsDTO = JobsDTO()
                        jobsDTO.staffID = cursor.getString(cursor.getColumnIndex(STAFF_ID))
                        jobsDTO.firstLastName =
                            cursor.getString(cursor.getColumnIndex(NAME_FIRST_LAST))
                        jobsDTO.price = cursor.getString(cursor.getColumnIndex(PRICE))
                        jobsDTO.rows = cursor.getString(cursor.getColumnIndex(ROW_SELECT_COUNT))
                        jobsDTO.jobType = cursor.getString(cursor.getColumnIndex(JOB_TYPE))
                        jobsDTO.rateType = cursor.getString(cursor.getColumnIndex(RATE_TYPE))
                        jobDTOArrayList.add(jobsDTO)
                    } while (cursor.moveToNext())
                }
            }
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        } finally {
            cursor?.close()
            close()
        }
        return jobDTOArrayList
    }

    /*
* @exitsStaffIDForJob()
* check is staff id exist in database
* */
    @Throws(SQLiteConstraintException::class)
    fun exitsStaffIDForJob(staffIDDatabase: String?, jobType: String?): Boolean {
        var staffID: String?
        var b = false
        open()
        var cursor: Cursor? = null
        try {
            cursor = mSqLiteDatabase!!.rawQuery(
                "SELECT $STAFF_ID FROM $TABLE_JOBS WHERE $STAFF_ID =? AND $JOB_TYPE =?",
                arrayOf(staffIDDatabase, jobType)
            )
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    do {
                        staffID = cursor.getString(cursor.getColumnIndex(STAFF_ID))
                        if (staffID == staffIDDatabase) {
                            b = true
                        }
                    } while (cursor.moveToNext())
                }
            }
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        } finally {
            cursor?.close()
            close()
        }
        return b
    }

    /*
* @getRowList()
* get row data from database based on jobtype and staffId
* */
    @Throws(SQLiteConstraintException::class)
    fun getRowList(jobType: String?, staffIDDatabase: String?): ArrayList<JobsDTO>? {
        val jobDTOArrayList: ArrayList<JobsDTO> = ArrayList<JobsDTO>()
        open()
        var cursor: Cursor? = null
        try {
            cursor = mSqLiteDatabase!!.rawQuery(
                "SELECT * FROM $TABLE_ROWS WHERE $JOB_TYPE =? AND $STAFF_ID=?",
                arrayOf(jobType, staffIDDatabase)
            )
            if (cursor != null) {
                if (cursor.count != 0) {
                    cursor.moveToFirst()
                    do {
                        val jobsDTO = JobsDTO()
                        jobsDTO.staffID = cursor.getString(cursor.getColumnIndex(STAFF_ID))
                        jobsDTO.rowID = cursor.getString(cursor.getColumnIndex(ROW_ID))
                        jobsDTO.maxTrees = cursor.getInt(cursor.getColumnIndex(MAX_TRESS))
                        jobsDTO.alreadyThinned =
                            cursor.getString(cursor.getColumnIndex(ALREADY_THINNED))
                        jobsDTO.alreadyPruned =
                            cursor.getString(cursor.getColumnIndex(ALREADY_PRUNED))
                        jobsDTO.assign = cursor.getString(cursor.getColumnIndex(ASSIGN))
                        jobsDTO.jobType = cursor.getString(cursor.getColumnIndex(JOB_TYPE))
                        jobsDTO.name = cursor.getString(cursor.getColumnIndex(NAME))
                        jobDTOArrayList.add(jobsDTO)
                    } while (cursor.moveToNext())
                }
            }
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        } finally {
            cursor?.close()
            close()
        }
        return jobDTOArrayList
    }

    /*
* @rowCountForStaff()
* get total cursor count for staff
* */
    @Throws(SQLiteConstraintException::class)
    fun rowCountForStaff(staffIDDatabase: String?): Int? {
        var count: Int? = null
        open()
        var cursor: Cursor? = null
        try {
            cursor = mSqLiteDatabase!!.rawQuery(
                "SELECT * FROM $TABLE_ROWS WHERE $STAFF_ID =?", arrayOf(staffIDDatabase)
            )
            if (cursor != null) {
                count = cursor.count
            }
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        } finally {
            cursor?.close()
            close()
        }
        return count
    }

    //open mSqLiteDatabase
    private fun open() {
        try {
            mSqLiteDatabase = this.readableDatabase
        } catch (openException: java.lang.Exception) {
            openException.printStackTrace()
        }
    }

    // close mSqLiteDatabase
    override fun close() {
        try {
            if (mSqLiteDatabase != null) {
                mSqLiteDatabase!!.close()
            }
        } catch (closeException: java.lang.Exception) {
            closeException.printStackTrace()
        }
    }
}