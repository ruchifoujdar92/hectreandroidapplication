package com.hectreandroidapp

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Paint
import android.graphics.drawable.ShapeDrawable
import android.graphics.drawable.shapes.RectShape
import android.os.Build
import android.text.InputType
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.View.OnTouchListener
import android.view.ViewGroup
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView


class RecyclerViewAdapter(
    private val context: Context,
    private val DTOArrayList: ArrayList<JobsDTO>,
    private val activity: JobActivity
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder?>() {
    private var map = HashMap<Int, HashMap<String, String>>()
    private var mapRow = HashMap<Int, HashMap<String, ArrayList<String?>>>()
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): RecyclerView.ViewHolder {
        return (when (viewType) {
            TYPE_ITEM -> {
                //Inflating recycle view item layout
                val itemView: View = LayoutInflater.from(parent.context)
                    .inflate(R.layout.custom_adpater_layout, parent, false)
                ItemViewHolder(itemView)
            }
            TYPE_FOOTER -> {
                //Inflating footer view
                val itemView: View = LayoutInflater.from(parent.context)
                    .inflate(R.layout.footer_layout, parent, false)
                FooterViewHolder(itemView)
            }
            else -> null
        })!!
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onBindViewHolder(
        holder: RecyclerView.ViewHolder,
        position: Int
    ) {
        val database = Database.getInstance(context)
        if (holder is ItemViewHolder) {
            //application of elvis operator(?:) if name is null it will execute right hand side value which is "name"
            val name = DTOArrayList[position].firstLastName
            holder.first_last_name_tv.text = name ?: "Name"
            holder.first_letter_name_tv.text = name?.get(0).toString()
            holder.price_et.setText(DTOArrayList[position].price ?: "0")
            val rateType = DTOArrayList[position].rateType ?: "RateType"
            val jobType = DTOArrayList[position].jobType ?: "JobType"
            val staffID = DTOArrayList[position].staffID ?: "StaffID"

            var mapAssignUnAssignRow = HashMap<String, ArrayList<String?>>()
            var arrayListRowAssign = ArrayList<String?>()
            var arrayListRowUnAssign = ArrayList<String?>()
            var map1 = HashMap<String, String>()

            val arrayList = database.getRowList(jobType, staffID)
            if (arrayList != null) {
                for (i in 0 until arrayList.size) {
                    val rowIDLocal = arrayList[i].rowID
                    val maxTress: Int? = arrayList[i].maxTrees
                    val alreadyPruned: String? = arrayList[i].alreadyPruned
                    val alreadyThinned: String? = arrayList[i].alreadyThinned
                    val assign: String? = arrayList[i].assign
                    val name: String? = arrayList[i].name

                    val btnRow = Button(context)
                    btnRow.layoutParams = LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.MATCH_PARENT
                    )
                    btnRow.text = "$rowIDLocal"

                    val drawable = ContextCompat.getDrawable(context, R.drawable.ic_orance_dot_10dp)
                    var count = 1

                    val tvLabel = TextView(context)

                    val edtTextFiled = EditText(context)
                    edtTextFiled.inputNumbersOnly()
                    val shape = ShapeDrawable(RectShape())
                    shape.paint.style = Paint.Style.STROKE
                    shape.paint.strokeWidth = 3f
                    edtTextFiled.background = shape;
                    val numberOfRowsTv = TextView(context)
                    numberOfRowsTv.text = "Number of trees for row $rowIDLocal"
                    tvLabel.text = name
                    if (assign == Assign.YES.value) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            btnRow.setBackgroundColor(
                                context.resources.getColor(
                                    R.color.buttonSelect,
                                    null
                                )
                            )
                            btnRow.setTextColor(
                                context.resources.getColor(
                                    R.color.textColorSelect,
                                    null
                                )
                            )
                        }
                        if (jobType == "job_pruning") {
                            setTextOnView(rowIDLocal, alreadyPruned, maxTress, edtTextFiled)
                        } else if (jobType == "job_thinning") {
                            setTextOnView(rowIDLocal, alreadyThinned, maxTress, edtTextFiled)
                        }
                        holder.linearLayout_textField.addView(numberOfRowsTv)
                        holder.linearLayout_textField.addView(edtTextFiled)
                        holder.linearLayout_textField.addView(tvLabel)
                        count = 2
                    } else if (assign == Assign.NO.value) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            btnRow.setBackgroundColor(
                                context.resources.getColor(
                                    R.color.buttonNotSelect,
                                    null
                                )
                            )
                            btnRow.setTextColor(
                                context.resources.getColor(
                                    R.color.textColorNotSelect,
                                    null
                                )
                            )
                        }
                    } else {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            btnRow.setBackgroundColor(
                                context.resources.getColor(
                                    R.color.buttonNotSelect,
                                    null
                                )
                            )
                            btnRow.setTextColor(
                                context.resources.getColor(
                                    R.color.textColorNotSelect,
                                    null
                                )
                            )
                        }
                    }
                    btnRow.setOnClickListener {
                        //Toast.makeText(context, "button pressed: $rowIDLocal", Toast.LENGTH_SHORT).show()
                        if (count % 2 == 0) {
                            //even
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                btnRow.setBackgroundColor(
                                    context.resources.getColor(
                                        R.color.buttonNotSelect,
                                        null
                                    )
                                )
                                btnRow.setTextColor(
                                    context.resources.getColor(
                                        R.color.textColorNotSelect,
                                        null
                                    )
                                )
                            }
                            holder.linearLayout_textField.removeView(numberOfRowsTv)
                            holder.linearLayout_textField.removeView(edtTextFiled)
                            holder.linearLayout_textField.removeView(tvLabel)
                            arrayListRowUnAssign.add(rowIDLocal)
                            mapAssignUnAssignRow[Assign.NO.value] = arrayListRowUnAssign

                        } else {
                            //odd
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                btnRow.setBackgroundColor(
                                    context.resources.getColor(
                                        R.color.buttonSelect,
                                        null
                                    )
                                )
                                btnRow.setTextColor(
                                    context.resources.getColor(
                                        R.color.textColorSelect,
                                        null
                                    )
                                )
                            }
                            if (jobType == "job_pruning") {
                                setTextOnView(rowIDLocal, alreadyPruned, maxTress, edtTextFiled)
                            } else if (jobType == "job_thinning") {
                                setTextOnView(rowIDLocal, alreadyThinned, maxTress, edtTextFiled)
                            }

                            holder.linearLayout_textField.addView(numberOfRowsTv)
                            holder.linearLayout_textField.addView(edtTextFiled)
                            holder.linearLayout_textField.addView(tvLabel)

                            arrayListRowAssign.add(rowIDLocal)
                            mapAssignUnAssignRow[Assign.YES.value] = arrayListRowAssign
                        }
                        count++
                        mapRow[position] = mapAssignUnAssignRow
                    }
                    if (rowIDLocal.equals("2")) {
                        btnRow.setCompoundDrawablesWithIntrinsicBounds(null, drawable, null, null)
                    }
                    holder.linearLayout_row_button.addView(btnRow)
                }
            }

            when {
                rateType == "price" -> {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        holder.price_rate_btn_select.setBackgroundColor(
                            context.resources.getColor(
                                R.color.buttonPriceWagesSelect,
                                null
                            )
                        )
                        holder.price_rate_btn_select.setTextColor(
                            context.resources.getColor(
                                R.color.textColorSelect,
                                null
                            )
                        )
                    }
                    holder.rate_tv.text = "Rate"
                    holder.linear_layout.visibility = View.VISIBLE
                }
                rateType == "wages" -> {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        holder.wages_btn_select.setBackgroundColor(
                            context.resources.getColor(
                                R.color.buttonPriceWagesSelect,
                                null
                            )
                        )
                        holder.wages_btn_select.setTextColor(
                            context.resources.getColor(
                                R.color.textColorSelect,
                                null
                            )
                        )
                        holder.rate_tv.setTextColor(
                            context.resources.getColor(
                                R.color.textColorRateWages,
                                null
                            )
                        )
                    }
                    holder.linear_layout.visibility = View.INVISIBLE
                    holder.rate_tv.text =
                        "Canker Removal(Job Name) will be paid by wages in this timesheet"

                }
                else -> {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        holder.price_rate_btn_select.setBackgroundColor(
                            context.resources.getColor(
                                R.color.buttonNotSelect,
                                null
                            )
                        )
                        holder.wages_btn_select.setBackgroundColor(
                            context.resources.getColor(
                                R.color.buttonNotSelect,
                                null
                            )
                        )
                        holder.wages_btn_select.setTextColor(
                            context.resources.getColor(
                                R.color.textColorNotSelect,
                                null
                            )
                        )
                        holder.price_rate_btn_select.setTextColor(
                            context.resources.getColor(
                                R.color.textColorNotSelect,
                                null
                            )
                        )
                    }
                    holder.linear_layout.visibility = View.INVISIBLE
                    holder.rate_tv.visibility = View.INVISIBLE
                }
            }

            holder.price_rate_btn_select.setOnClickListener {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    holder.price_rate_btn_select.setBackgroundColor(
                        context.resources.getColor(
                            R.color.buttonPriceWagesSelect,
                            null
                        )
                    )
                    holder.wages_btn_select.setBackgroundColor(
                        context.resources.getColor(
                            R.color.buttonNotSelect,
                            null
                        )
                    )
                    holder.price_rate_btn_select.setTextColor(
                        context.resources.getColor(
                            R.color.textColorSelect,
                            null
                        )
                    )
                    holder.wages_btn_select.setTextColor(
                        context.resources.getColor(
                            R.color.textColorNotSelect,
                            null
                        )
                    )
                }
                holder.linear_layout.visibility = View.VISIBLE
                holder.rate_tv.visibility = View.VISIBLE
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    holder.rate_tv.setTextColor(
                        context.resources.getColor(
                            R.color.textColorNotSelect,
                            null
                        )
                    )
                }
                holder.rate_tv.text = "Rate"
                map1["rateType"] = "price"
                map[position] = map1
            }

            holder.wages_btn_select.setOnClickListener {
                holder.linear_layout.visibility = View.INVISIBLE
                holder.rate_tv.visibility = View.VISIBLE
                //even
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    holder.price_rate_btn_select.setBackgroundColor(
                        context.resources.getColor(
                            R.color.buttonNotSelect,
                            null
                        )
                    )
                    holder.wages_btn_select.setBackgroundColor(
                        context.resources.getColor(
                            R.color.buttonPriceWagesSelect,
                            null
                        )
                    )
                    holder.wages_btn_select.setTextColor(
                        context.resources.getColor(
                            R.color.textColorSelect,
                            null
                        )
                    )
                    holder.price_rate_btn_select.setTextColor(
                        context.resources.getColor(
                            R.color.textColorNotSelect,
                            null
                        )
                    )
                    holder.rate_tv.setTextColor(
                        context.resources.getColor(
                            R.color.textColorRateWages,
                            null
                        )
                    )
                }
                holder.rate_tv.text =
                    "Canker Removal(Job Name) will be paid by wages in this timesheet"
                map1["rateType"] = "wages"
                map[position] = map1
            }
            holder.apply_to_all_tv.setOnClickListener {
                val jobDTO = JobsDTO()
                jobDTO.price = holder.price_et.text.toString()
                jobDTO.jobType = jobType
                database.applyToAllStaff(jobDTO)
                Toast.makeText(context, "Applied to all", Toast.LENGTH_SHORT).show()
            }
        } else if (holder is FooterViewHolder) {
            holder.confirm_btn.setOnClickListener {
                val jobDTO = JobsDTO()
                //updating rate_type
                for (key in map.keys) {
                    jobDTO.jobType = DTOArrayList[key].jobType
                    jobDTO.staffID = DTOArrayList[key].staffID
                    for (values in map[key]!!) {
                        if (values.key == "rateType") {
                            jobDTO.rateType = values.value
                        }
                        if (values.key == "price") {
                            jobDTO.price = values.value
                        }
                    }
                    database.updateJob(jobDTO)
                }
                //updating row data
                for (key in mapRow.keys) {
                    jobDTO.jobType = DTOArrayList[key].jobType
                    jobDTO.staffID = DTOArrayList[key].staffID
                    for (values in mapRow[key]!!) {
                        for (rowId in values.value) {
                            if (values.key == Assign.YES.value) {
                                jobDTO.assign = Assign.YES.value
                                jobDTO.rowID = rowId
                            } else if (values.key == Assign.NO.value) {
                                jobDTO.assign = Assign.NO.value
                                jobDTO.rowID = rowId
                            }
                            database.updateRow(jobDTO)
                        }
                    }
                }
                Toast.makeText(context, "Records are updated", Toast.LENGTH_SHORT).show()
                activity.finish()
            }
        }
    }

    // extension function to set edit text multiple input types
// allow to input positive and negative decimal and integer numbers
    private fun EditText.inputNumbersOnly() {
        inputType =
            InputType.TYPE_CLASS_NUMBER or // allow numbers
                    InputType.TYPE_NUMBER_FLAG_DECIMAL or // allow decimal numbers
                    InputType.TYPE_NUMBER_FLAG_SIGNED // allow positive and negative numbers
    }

    override fun getItemViewType(position: Int): Int {
        if (position == (DTOArrayList.size)) {
            return TYPE_FOOTER
        }
        return TYPE_ITEM
    }

    override fun getItemCount(): Int {
        return (DTOArrayList.size) + 1
    }

    private inner class FooterViewHolder(view: View) :
        RecyclerView.ViewHolder(view) {
        var confirm_btn: Button = view.findViewById(R.id.confirm_btn)

    }

    private inner class ItemViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        val first_letter_name_tv: TextView
        val first_last_name_tv: TextView
        val rate_tv: TextView
        val apply_to_all_tv: TextView
        val price_et: EditText
        val price_rate_btn_select: Button
        val wages_btn_select: Button
        val linear_layout: LinearLayout
        val linearLayout_row_button: LinearLayout
        val linearLayout_textField: LinearLayout

        init {
            this.first_letter_name_tv = itemView.findViewById(R.id.first_letter_name_tv)
            this.first_last_name_tv = itemView.findViewById(R.id.first_last_name_tv)
            this.rate_tv = itemView.findViewById(R.id.rate_tv)
            this.price_et = itemView.findViewById(R.id.price_et)
            this.apply_to_all_tv = itemView.findViewById(R.id.apply_to_all_tv)
            this.price_rate_btn_select = itemView.findViewById(R.id.price_rate_btn_select)
            this.wages_btn_select = itemView.findViewById(R.id.wages_btn_select)
            this.linear_layout = itemView.findViewById(R.id.linear_layout)
            this.linearLayout_row_button = itemView.findViewById(R.id.linearLayout_row_button)
            this.linearLayout_textField = itemView.findViewById(R.id.linearLayout_textField)
        }
    }

    companion object {
        private const val TYPE_FOOTER = 1
        private const val TYPE_ITEM = 2
    }

    // used to set text on a textview
    @SuppressLint("SetTextI18n")
    private fun setTextOnView(
        rowIDLocal: String?,
        completeAlready: String?,
        maxTress: Int?,
        tvDynamic: EditText
    ) {
        when (rowIDLocal) {
            "1" -> {
                tvDynamic.setText("$completeAlready/$maxTress")
            }
            "2" -> {
                tvDynamic.setText("$completeAlready/$maxTress")
            }
            "3" -> {
                tvDynamic.setText("$completeAlready/$maxTress")
            }
        }
    }
}