package com.hectreandroidapp

import android.app.Dialog
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.Window
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView


class JobActivity : AppCompatActivity() {
    var jobCall: String = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_job)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        var adapter: RecyclerViewAdapter? = null
        val database = Database.getInstance(this)
        val recyclerView = findViewById<RecyclerView>(R.id.recyclerview)
        recyclerView.addItemDecoration(
            DividerItemDecoration(
                this,
                LinearLayoutManager.HORIZONTAL
            )
        )

        val intent = intent;
        jobCall = intent.getStringExtra("call")

        if (jobCall == "job_pruning") {
            supportActionBar!!.title = "Pruning"
            var jobsDTO = addJobData("Barco Isabella", "35", "2", "1", jobCall, "price")
            val names = arrayOf("Yi Wan(250)", "Elizabeth Jargrave(100)", "John Arrown(300)")
            for (i in 1..2) {
                val jobsDTORows = addRowData(i, 256, "1", jobCall, names[i - 1])
                val count = database.rowCountForStaff("1")
                if (count!! < 2) {
                    database.addRow(jobsDTORows)
                } else {
                    break
                }
            }

            if (!database.exitsStaffIDForJob("1", jobCall)) {
                database.addJob(jobsDTO)
            }
            jobsDTO = addJobData("Henry Pham", "35", "3", "2", jobCall, "wages")
            for (i in 1..3) {
                val jobsDTORows = addRowData(i, 256, "2", jobCall, names[i - 1])
                val count = database.rowCountForStaff("2")
                if (count!! < 3) {
                    database.addRow(jobsDTORows)
                } else {
                    break
                }
            }
            if (!database.exitsStaffIDForJob("2", jobCall)) {
                database.addJob(jobsDTO)
            }
            val arrayList = database.getJobList(jobCall)
            adapter = arrayList?.let { RecyclerViewAdapter(this, it, this@JobActivity) }
        } else if (jobCall == "job_thinning") {
            supportActionBar!!.title = "Thinning"
            val jobsDTO = addJobData("Darijian Olivia", "35", "2", "3", jobCall, "price")
            val names = arrayOf("Yi Wan(250)", "Elizabeth Jargrave(100)")
            for (i in 1..2) {
                val jobsDTORows = addRowData(i, 256, "3", jobCall, names[i - 1])
                val count = database.rowCountForStaff("3")
                if (count!! < 2) {
                    database.addRow(jobsDTORows)
                } else {
                    break
                }
            }

            if (!database.exitsStaffIDForJob("3", jobCall)) {
                database.addJob(jobsDTO)
            }
            val arrayList = database.getJobList(jobCall)
            adapter = arrayList?.let { RecyclerViewAdapter(this, it, this@JobActivity) }
        }
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = adapter
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.options_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.add_max_trees -> {
                // When User click on dialog button, call this method
                showDialog(jobCall)
                true
            }
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    // used to show dialog box when click on Add to mAx tree button
    private fun showDialog(jobCall: String) {
        val database = Database.getInstance(this)
        val jobsDTO = JobsDTO()
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.custom_alert_dialog)
        val enter_max_tress_et = dialog.findViewById(R.id.enter_max_tress_et) as EditText
        val enter_row_et = dialog.findViewById(R.id.enter_row_et) as EditText
        val close_tv = dialog.findViewById(R.id.close_tv) as TextView

        val confrimBtn = dialog.findViewById(R.id.confirm_btn) as Button
        confrimBtn.setOnClickListener {
            jobsDTO.maxTrees = enter_max_tress_et.text.toString().toInt()
            jobsDTO.rowID = enter_row_et.text.toString()
            jobsDTO.jobType = jobCall
            database.addMaxTrees(jobsDTO)
            dialog.dismiss()
        }
        close_tv.setOnClickListener { dialog.dismiss() }
        dialog.show()
    }

    //adding job data to table_job
    private fun addJobData(
        firstLastName: String, price: String, rowCount: String,
        staffID: String, jobCall: String, rateType: String
    ): JobsDTO {
        val jobsDTO = JobsDTO()
        jobsDTO.firstLastName = firstLastName
        jobsDTO.price = price
        jobsDTO.rows = rowCount
        jobsDTO.staffID = staffID
        jobsDTO.jobType = jobCall
        jobsDTO.rateType = rateType
        return jobsDTO
    }

    //adding row data to table_row
    private fun addRowData(
        row: Int,
        maxTrees: Int,
        staffID: String,
        jobCall: String,
        name: String
    ): JobsDTO {
        val jobsDTO = JobsDTO()
        jobsDTO.rowID = row.toString()
        jobsDTO.maxTrees = maxTrees.plus(row)
        jobsDTO.name = name
        if (jobCall == "job_pruning") {
            jobsDTO.alreadyPruned = row.toString()
        } else if (jobCall == "job_thinning") {
            jobsDTO.alreadyThinned = row.toString()
        }
        jobsDTO.staffID = staffID
        jobsDTO.jobType = jobCall
        when (row) {
            1 -> {
                jobsDTO.assign = Assign.NO.value
            }
            2 -> {
                jobsDTO.assign = Assign.YES.value
            }
            3 -> {
                jobsDTO.assign = Assign.YES.value
            }
        }
        return jobsDTO
    }
}
