package com.hectreandroidapp

enum class Assign(val value: String) {
    YES("1"),
    NO("2"),
}