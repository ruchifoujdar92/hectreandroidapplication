package com.hectreandroidapp

class JobsDTO{

    internal var firstLastName: String? = null
    internal var price:String? = null
    internal var rows:String? = null
    internal var staffID:String? = null
    internal var jobType:String? = null
    internal var rateType:String? = null
    internal var rowID:String? = null
    internal var maxTrees:Int? = null
    internal var alreadyPruned:String? = null
    internal var alreadyThinned:String? = null
    internal var assign:String? = null
    internal var name:String? = null

}