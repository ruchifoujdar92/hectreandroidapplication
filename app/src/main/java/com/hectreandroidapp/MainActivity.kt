package com.hectreandroidapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun onClick(view: View?) {
        when (view?.id) {
            R.id.job_pruning -> {
                intent = Intent(this, JobActivity::class.java)
                intent.putExtra("call", "job_pruning")
                startActivity(intent)
            }
            R.id.job_thinning -> {
                intent = Intent(this, JobActivity::class.java)
                intent.putExtra("call", "job_thinning")
                startActivity(intent)
            }
        }
    }
}
